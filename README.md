# Symfony 5 Docker template
This repository contains docker setup to work with a Symfony 5 application.

Containers are:
- php-fpm
- nginx
- SQL database (mariadb)

## How to use

Create your Symfony project and copy / paste the contents of the repo in your project root directory.

Database is created automatically at startup and the data is stored in `.docker/database/data`

To define database name & credentials you need to add the following env variables:
```
#### Docker env #####
DATABASE_NAME=sf
DATABASE_USER=user
DATABASE_PASSWORD=user
DATABASE_ROOT_PASSWORD=root
```

You can find them in `.env` file.
